
package Controlador;

import Mysql.MySQL;
import Vista.Menubalancegeneral;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;

public class BalanceController {
   Connection conn;
   Menubalancegeneral interfaz;
   ArrayList<Object[]> lista;
   public BalanceController(Menubalancegeneral GUI){
       lista = new ArrayList<Object[]>();
       interfaz=GUI;
        conn= MySQL.getConexion();
       
   }
   
public void obtenerCuentas() throws SQLException{
    String sql="select cuentas.nombre, sum(movimientos.monto), cuentas.naturaleza from cuentas, movimientos where cuentas.id=movimientos.id_cuenta group by cuentas.nombre;";
    Object[] s= new Object[3];
    PreparedStatement pstm = conn.prepareStatement(sql);
    ResultSet rs = pstm.executeQuery();
            while(rs.next()){
               s[0]=rs.getString(1);
               s[1]=rs.getInt(2);
               s[2]=rs.getString(3);
               lista.add(s);
              // System.out.print(s[0]+s[1]+s[2]);
            }
}
   public void tableActivo(){
       Object[] datos= new Object[2];
       for(Object[] u: lista){
          // for(Object x: u){
               if("activo-circulante".equals(u[2])){
                   datos[0]=u[0];
                   datos[1]=u[1];
                   
               }
               if("activo-fijo".equals(u[2])){
                   datos[0]=u[0];
                   datos[1]=u[1];
                   
               }
                if("activo-diferido".equals(u[2])){
                   datos[0]=u[0];
                   datos[1]=u[1];
                 //  interfaz.model.addRow(datos);
               }
                interfaz.model.addRow(datos);
                datos[0]="";
                datos[1]="";
        //   }
       }
   }
   
   public void tablePasivo(){
       Object[] datos= new Object[2];
       for(Object[] u: lista){
        //   for(Object x: u){
               if("pasivo-circulante".equals(u[2])){
                   datos[0]=u[0];
                   datos[1]=u[1];
                 //  interfaz.model2.addRow(datos);
               }
               if("pasivo-fijo".equals(u[2])){
                   datos[0]=u[0];
                   datos[1]=u[1];
                   //interfaz.model2.addRow(datos);
               }
                if("pasivo-diferido".equals(u[2])){
                   datos[0]=u[0];
                   datos[1]=u[1];
                  
               }
                 interfaz.model2.addRow(datos);
                datos[0]="";
                datos[1]="";
           }
       //}
   }
   
   public void totalActivo(){
       double activo=0;
       for(Object[] u: lista){
           //for(Object x: u){
               if("activo-circulante".equals(u[2])){
               activo=activo+(Double.parseDouble(u[1].toString()));
               }
               if("activo-fijo".equals(u[2])){
             activo=activo+(Double.parseDouble(u[1].toString()));
               }
                if("activo-diferido".equals(u[2])){
                  activo=activo+(Double.parseDouble(u[1].toString()));
               }
           //}
           interfaz.t1.setText(String.valueOf(activo));
   }
   }
   
    public void totalPasivo(){
           double  pasivo=0;
       for(Object[] u: lista){
           for(Object x: u){
               if("pasivo-circulante".equals(u[2])){
               pasivo=pasivo+(Double.parseDouble(u[1].toString()));
               }
               if("pasivo-fijo".equals(u[2])){
             pasivo=pasivo+(Double.parseDouble(u[1].toString()));
               }
                if("pasivo-diferido".equals(u[2])){
                  pasivo=pasivo+(Double.parseDouble(u[1].toString()));
               }
           }
           interfaz.t2.setText(String.valueOf(pasivo));
     }
    }
    
    public void totalCapital(){
          double  pasivo=0;
       for(Object[] u: lista){
           for(Object x: u){
               if("capital".equals(u[2])){
               pasivo=pasivo+(Double.parseDouble(u[1].toString()));
               }
            
           }
           interfaz.t3.setText(String.valueOf(pasivo));
           System.out.println();
     }
   }
   
   
   
   
public int obtenerId2(String s) throws SQLException{
                
            String sql ="select id from cuentas where nombre=?;";
            int item=0;
            PreparedStatement pstm = conn.prepareStatement(sql);
            pstm.setString(1, s );
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
               item=Integer.parseInt(rs.getString(1)); 
            }
        return item;
    
}
    public String obtenerId(int num) throws SQLException{
                
            String sql ="select nombre from cuentas where id=?;";
            String res="";
            PreparedStatement pstm = conn.prepareStatement(sql);
            pstm.setInt(1, num);
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
               res=rs.getString(1); 
            }
            return res;
    }
    
    public void run() throws SQLException{
        obtenerCuentas();
        tableActivo();
        tablePasivo();
        totalActivo();
        totalPasivo();
        totalCapital();
    }
    
}
