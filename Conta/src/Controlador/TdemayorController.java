
package Controlador;
import Mysql.MySQL;
import Vista.Menutdemayor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Memo
 */
public class TdemayorController {
    Connection conn;
    Menutdemayor interfaz;
    ArrayList<String> lista;
    public TdemayorController(Menutdemayor GUI){
    interfaz=GUI;
    lista = new ArrayList<String>();
   conn= MySQL.getConexion();
    }
    
    public void obtenerCuentas()
        throws SQLException{
                
            String sql ="select nombre from cuentas";
            
            PreparedStatement pstm = conn.prepareStatement(sql);
            //pstm.setString(1, interfaz.cuenta.getSelectedItem().toString() );
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
             lista.add(rs.getString(1)); 
            }
    }
    
    public void generaT() throws SQLException{
        obtenerCuentas();
         String datos[] = new String[5];
         String datos2[] = new String[4];
         String datos3[] = new String[4];

          
                  
        for(String u: lista){
             interfaz.model.addRow(datos3); 
            String sql ="select id_cuenta, monto, debehaber from cuentas.movimientos where id_cuenta=? ";
            PreparedStatement pstm = conn.prepareStatement(sql);
            pstm.setInt(1, obtenerId2(u));
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
                datos[0]=rs.getString(1);
                datos[1]=rs.getString(2);
                datos[2]=rs.getString(3);
                datos[0]=obtenerId(Integer.parseInt(datos[0]));
               
                if("Haber".equals(datos[2])){
                     System.out.print(datos[2]);
                    datos2[2]=datos[0];
                    datos2[3]=datos[1];
                    datos2[0]=null;
                    datos2[1]=null;
                }else {
                    datos2[0]=datos[0];
                    datos2[1]=datos[1];
                    datos2[2]=null;
                    datos2[3]=null;
                }
                
                
                interfaz.model.addRow(datos2);
            }
        }
    }
public int obtenerId2(String s) throws SQLException{
                
            String sql ="select id from cuentas where nombre=?;";
            int item=0;
            PreparedStatement pstm = conn.prepareStatement(sql);
            pstm.setString(1, s );
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
               item=Integer.parseInt(rs.getString(1)); 
            }
        return item;
    
}
    public String obtenerId(int num) throws SQLException{
                
            String sql ="select nombre from cuentas where id=?;";
            String res="";
            PreparedStatement pstm = conn.prepareStatement(sql);
            pstm.setInt(1, num);
            ResultSet rs = pstm.executeQuery();
            while(rs.next()){
               res=rs.getString(1); 
            }
            return res;
    }
    
}
